/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ccre.igneous;

/**
 * A class that holds the current Igneous Launcher. This is done because the
 * launcher variable must be set before the Igneous class is initialized.
 *
 * @author skeggsc
 */
class IgneousLauncherHolder {

    static IgneousLauncher launcher;

}
