This is the Common Chicken Runtime Engine core, Poultry Inspector, and Igneous system.
It will hopefully form the basis for all of Team 1540's robotics-related java programming.

Go [here](https://bitbucket.org/col6y/common-chicken-runtime-engine/wiki) to see the main documentation.

The old documentation can be found in DOCUMENTATION.MD under the SOURCE tab.

To contact the primary author, Colby Skeggs, write email to skeggsc [at] catlin [dot] edu.
