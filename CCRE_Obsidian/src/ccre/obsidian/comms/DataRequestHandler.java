/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ccre.obsidian.comms;

import java.io.OutputStream;

/**
 *
 * @author MillerV
 */
public interface DataRequestHandler {
    public void onRequest(OutputStream os);
}
