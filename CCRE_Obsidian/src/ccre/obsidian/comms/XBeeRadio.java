/*
 * Copyright 2013 Vincent Miller
 * 
 * This file is part of the CCRE, the Common Chicken Runtime Engine.
 * 
 * The CCRE is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * The CCRE is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CCRE.  If not, see <http://www.gnu.org/licenses/>.
 */
package ccre.obsidian.comms;

import ccre.log.LogLevel;
import ccre.log.Logger;
import com.rapplogic.xbee.api.PacketListener;
import com.rapplogic.xbee.api.XBee;
import com.rapplogic.xbee.api.XBeeAddress64;
import com.rapplogic.xbee.api.XBeeException;
import com.rapplogic.xbee.api.XBeeRequest;
import com.rapplogic.xbee.api.zigbee.ZNetTxRequest;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;

/**
 *
 * @author MillerV
 */
public class XBeeRadio {

    private final XBee xbee;
    private String port;
    private int baudRate;
    private long[] timeouts;
    private XBeeRequest[] messages;
    private final BlockingQueue<XBeeRequest> sendQueue;

    public XBeeRadio(String port, int baudRate) {
        this.port = port;
        this.baudRate = baudRate;
        this.xbee = new XBee();
        this.sendQueue = new LinkedBlockingQueue<XBeeRequest>();
        
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                    XBeeRequest message = sendQueue.take();
                    xbee.sendAsynchronous(message);
                    } catch (InterruptedException e) {
                        Logger.log(LogLevel.SEVERE, "Error waiting for outgoing packets: ", e);
                    } catch (XBeeException e) {
                        Logger.log(LogLevel.SEVERE, "Error sending packet: ", e);
                    }
                }
            }
        }.start();
    }

    public void modify(String port, int baudRate) throws XBeeException {
        close();
        this.port = port;
        this.baudRate = baudRate;
        open();
    }

    public void open() throws XBeeException {
        xbee.open(port, baudRate);
    }

    public void close() {
        xbee.close();
    }

    public void sendPacket(int[] addr, int[] msg) throws XBeeException {
        //Logger.info("Sent: " + Arrays.toString(msg) + " to " + Arrays.toString(addr));
        XBeeAddress64 address = new XBeeAddress64(addr);
        ZNetTxRequest message = new ZNetTxRequest(address, msg);
        sendQueue.add(message);
    }

    public void sendBroadcast(int[] msg) throws XBeeException {
        //Logger.info("Broadcast: " + Arrays.toString(msg));
        ZNetTxRequest message = new ZNetTxRequest(XBeeAddress64.BROADCAST, msg);
        try {
            sendQueue.put(message);
        } catch (InterruptedException ex) {
            Logger.log(LogLevel.SEVERE, "Error waiting for outgoing message: ", ex);
        }
    }

    public void addPacketListener(PacketListener listener) {
        xbee.addPacketListener(listener);
    }
}
