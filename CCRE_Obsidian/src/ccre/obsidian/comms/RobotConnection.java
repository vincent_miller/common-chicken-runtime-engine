/*
 * Copyright 2013 Vincent Miller
 * 
 * This file is part of the CCRE, the Common Chicken Runtime Engine.
 * 
 * The CCRE is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * The CCRE is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CCRE.  If not, see <http://www.gnu.org/licenses/>.
 */
package ccre.obsidian.comms;

import ccre.channel.BooleanInput;
import ccre.channel.BooleanOutput;
import ccre.channel.BooleanStatus;
import ccre.channel.EventOutput;
import ccre.channel.EventInput;
import ccre.ctrl.BooleanMixing;
import ccre.ctrl.Ticker;
import ccre.log.LogLevel;
import ccre.log.Logger;
import ccre.obsidian.ObsidianCore;
import com.rapplogic.xbee.api.XBeeException;

/**
 *
 * @author millerv
 */
public class RobotConnection {
    // The pathetic one with the wire antenna.
    //public static final int[] home = new int[]{0x00, 0x13, 0xA2, 0x00, 0x40, 0xA8, 0xC4, 0x10};

    public static BooleanStatus enabled = new BooleanStatus();

    public static boolean alive = false;
    private static XBeeRadio radio;
    private static EventInput heartbeat;
    private static long timeSinceBeat = System.currentTimeMillis();

    public static int[] home = null;

    public static void startConnection(String port, int baud, final ObsidianCore notify) {
        XBeeRadio radio = new XBeeRadio(port, baud);
        try {
            radio.open();
            Logger.log(LogLevel.INFO, "Found radio");
        } catch (XBeeException e) {
            Logger.log(LogLevel.WARNING, "Could not connect", e);
        }
        ObsidianCommsNode.createGlobalNode(false, radio);

        heartbeat = ObsidianCommsNode.globalNode.createEventSource(CommsID.ID_EVENT_HEARTBEAT);
        enabled = new BooleanStatus();
        ObsidianCommsNode.globalNode.addListener(CommsID.ID_BOOL_ENABLED, enabled);

        ObsidianCommsNode.globalNode.setARPHandler(new ARPHandler() {
            @Override
            public void onResponse(int[] addr) {
                home = addr;
            }

            @Override
            public boolean onRequest(int[] addr) {
                return false;
            }
        });

        ObsidianCommsNode.globalNode.createEventSource(CommsID.ID_EVENT_AUTO).send(new EventOutput() {
            @Override
            public void event() {
                notify.mode = ObsidianCore.Mode.AUTO;
                notify.during = false;
            }
        });

        ObsidianCommsNode.globalNode.createEventSource(CommsID.ID_EVENT_TELEOP).send(new EventOutput() {
            @Override
            public void event() {
                notify.mode = ObsidianCore.Mode.TELEOP;
                notify.during = false;
            }
        });

        ObsidianCommsNode.globalNode.createEventSource(CommsID.ID_EVENT_IDLE).send(new EventOutput() {
            @Override
            public void event() {
                notify.mode = ObsidianCore.Mode.IDLE;
                notify.during = false;
            }
        });

        enabled.send(new BooleanOutput() {
            @Override
            public void set(boolean b) {
                if (b && !alive) {
                    notify.enabled.event();
                    alive = true;
                    Logger.info("alive");
                } else if (!b && alive) {
                    notify.disabled.event();
                    alive = false;
                    Logger.info("dead");

                }
            }
        });

        heartbeat.send(new EventOutput() {
            @Override
            public void event() {
                timeSinceBeat = System.currentTimeMillis();
                if (!alive && enabled.get()) {
                    notify.enabled.event();
                    alive = true;
                }
            }
        });

        new Ticker(20).send(new EventOutput() {
            @Override
            public void event() {
                if (System.currentTimeMillis() - timeSinceBeat > 1000 && alive) {
                    alive = false;
                    notify.disabled.event();
                }
            }
        });
    }
}
