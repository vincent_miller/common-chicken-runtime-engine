/*
 * Copyright 2013-2014 Colby Skeggs and Vincent Miller
 * 
 * This file is part of the CCRE, the Common Chicken Runtime Engine.
 * 
 * The CCRE is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * The CCRE is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CCRE.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.team1540.example;

<<<<<<< HEAD
import ccre.chan.FloatInput;
import ccre.chan.FloatOutput;
import ccre.event.EventConsumer;
import ccre.obsidian.GPIOChannels;
=======
import ccre.channel.*;
import ccre.ctrl.FloatMixing;
>>>>>>> 54e749488757d3ab55163ace752f4602412b05d2
import ccre.obsidian.ObsidianCore;
import ccre.obsidian.PWMPin;
import ccre.obsidian.comms.CommsID;
import ccre.obsidian.comms.ObsidianCommsNode;

public class Example extends ObsidianCore {

    @Override
    protected void createRobotControl() {
<<<<<<< HEAD
        final FloatInput xAxis = launcher.getJoystickAxis(CommsID.ID_FLOAT_JOYSTICK_X);
        final FloatInput yAxis = launcher.getJoystickAxis(CommsID.ID_FLOAT_JOYSTICK_Y);
=======
        final FloatInput xAxis = launcher.getJoystickAxis((byte) 1);
        final FloatInput yAxis = FloatMixing.negate(launcher.getJoystickAxis((byte) 2));
>>>>>>> 54e749488757d3ab55163ace752f4602412b05d2
        
        final FloatOutput leftMotor = makePWMOutput(PWMPin.P8_13, 0, 0.333f, 0.666f, 333f, true);
        final FloatOutput rightMotor = makePWMOutput(PWMPin.P9_14, 0, 0.333f, 0.666f, 333f, true);
        
<<<<<<< HEAD
        ObsidianCommsNode.globalNode.addListener(CommsID.ID_BOOL_JOYSTICK_B1, makeGPIOOutput(GPIOChannels.GPIO1_0, false));
        
        periodic.addListener(new EventConsumer() {
            @Override
            public void eventFired() {
                leftMotor.writeValue(yAxis.readValue() + xAxis.readValue());
                rightMotor.writeValue(yAxis.readValue() - xAxis.readValue());
=======
        periodic.send(new EventOutput() {
            @Override
            public void event() {
                //Logger.finest("Vals: " + xAxis.readValue() + ", " + yAxis.readValue());
                leftMotor.set(yAxis.get() + xAxis.get());
                rightMotor.set(yAxis.get() - xAxis.get());
                //rightMotor.writeValue(1.0f);                //leftMotor.writeValue(1.0f);
>>>>>>> 54e749488757d3ab55163ace752f4602412b05d2
            }
        });
    }
}
